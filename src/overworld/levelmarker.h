/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef LEVELMARKER_H
#define LEVELMARKER_H

#include "route.h"

#define COLOR_MARKER_HIDDEN Color::WHITE.Transparent()
#define COLOR_MARKER_INACTIVE Color{ 2/3.f }.Transparent(.9f)
#define COLOR_MARKER_ACTIVE Color::YELLOW.Transparent(.75f)

class LevelMarker: public Animatable
{
    DRY_OBJECT(LevelMarker, Animatable);

public:
    static void RegisterObject(Context* context);
    LevelMarker(Context* context, unsigned id, const Vector2& position, const String& map);
    LevelMarker(Context* context): LevelMarker(context, M_MAX_UNSIGNED, Vector2::ZERO, "")
    {}

    void SetScene(Scene* scene) { scene_ = scene; }
    float GetViewYaw() const;
    void FadeToColor(const Color& color, float delay = 0.f);

    void SetColor(const Color& color) { color_ = color; }
    Color GetColor() const { return color_; }

    unsigned GetID() const { return id_; }
    Vector3 GetPosition() const { return { position_.x_, 0.f, position_.y_ }; }
    String GetMapName() const { return map_; }

    void SetNext(LevelMarker* next) { next_ = next; }
    void SetPrevious(LevelMarker* previous) { previous_ = previous; }
    LevelMarker* GetNext() const { return next_; }
    LevelMarker* GetPrevious() const { return previous_; }

    void SetRoute(Route* route);

    Route* GetRoute() const { return route_; }
    bool HasRoute() const { return route_ != nullptr; }

    bool IsLocked() const { return locked_; }
    void Unlock();

    void Reset();

protected:
    void OnAttributeAnimationAdded() override;
    void OnAttributeAnimationRemoved() override;

private:
    void HandleAttributeAnimationUpdate(StringHash eventType, VariantMap& eventData);

    Scene* scene_;
    unsigned id_;
    Vector2 position_;
    String map_;
    Color color_;
    LevelMarker* next_;
    LevelMarker* previous_;
    Route* route_;
    bool locked_;
};

#endif // LEVELMARKER_H
