/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "route.h"

Route::Route(Context* context): Component(context),
    dashes_{ nullptr },
    spline_{ BEZIER_CURVE },
    length_{ 0.f },
    lastDash_{ 0.f },
    progress_{ -1.f },
    destination_{ M_MAX_UNSIGNED }
{}

void Route::OnNodeSet(Node* node)
{
    if (!node)
        return;

    dashes_ = node_->CreateComponent<DecalSet>();
    dashes_->SetMaterial(RES(Material, "Materials/Dash.xml"));
}

void Route::SetPoints(const PODVector<Vector3>& points)
{
    spline_.Clear();
    for (const Vector3& point: points)
        spline_.AddKnot(point);

    CalculateLength();
}

void Route::Appear()
{
    if (!GetScene())
        return;

    progress_ = 0.f;

    SubscribeToEvent(GetScene(), E_SCENEUPDATE, DRY_HANDLER(Route, HandleSceneUpdate));
}

void Route::Reset()
{
    dashes_->RemoveAllDecals();
    lastDash_ = 0.f;
    progress_ = -1.f;
}

Vector3 Route::GetPoint(float at) const
{
    return spline_.GetPoint(at / length_).GetVector3();
}

void Route::CalculateLength()
{
    length_ = 0.f;

    if (spline_.GetKnots().Size() <= 0)
        return;

    Vector3 a{ spline_.GetKnot(0).GetVector3() };
    const int r{ 100 };
    for (int i{ 0 }; i <= r; ++i)
    {
        const Vector3 b{ spline_.GetPoint(1.f * i / r).GetVector3() };
        length_ += (a - b).Length();
        a = b;
    }
}

void Route::HandleSceneUpdate(StringHash /*eventType*/, VariantMap& eventData)
{
    const float timeStep{ eventData[SceneUpdate::P_TIMESTEP].GetFloat() };

    if (progress_ == length_)
    {
        UnsubscribeFromEvent(GetScene(), E_SCENEUPDATE);

        VariantMap eventData{};
        eventData[RouteAppeared::P_ROUTE] = this;
        eventData[RouteAppeared::P_DESTINATION] = destination_;
        SendEvent(E_ROUTEAPPEARED, eventData);

        return;
    }

    if (progress_ < 0.f)
        return;

    float spacing{ .23f };
    const float clip{ 1/4.f };
    const float effectiveLength{ length_ - 2.f * clip };
    const int count{ FloorToInt(effectiveLength / spacing) };
    const float leap{ (effectiveLength - spacing * count) / effectiveLength };
    spacing += leap;

    progress_ = Min(length_, progress_ + 3.4f * timeStep);
    const bool nearEnd{ progress_ < clip || length_ - progress_ < clip };
    if (!nearEnd && progress_ > lastDash_ + spacing)
    {
        Node* islandNode{ node_->GetParent() };
        const Vector3 direction{ (GetPoint(progress_ + spacing) - GetPoint(progress_ - spacing)).Normalized() };
        const float angle{ islandNode->GetWorldRotation().YawAngle() + Asin(Vector3::FORWARD.CrossProduct(direction).y_) };
        const Vector3 pos{ islandNode->LocalToWorld(GetPoint(progress_)) };
        StaticModel* island{ islandNode->GetComponent<StaticModel>() };
        const Quaternion rotation{ Quaternion{ angle, Vector3::UP } * Quaternion{ 90.f, Vector3::RIGHT } };
        dashes_->AddDecal(island, { pos.x_, 3.f, pos.z_ }, rotation, spacing * .42f, .42f, 10.f, Vector2::ZERO, Vector2::ONE);

        lastDash_ = progress_;
    }
}
