/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "levelmarker.h"

void LevelMarker::RegisterObject(Context* context)
{
    context->RegisterFactory<LevelMarker>();
    context->RegisterFactory<Route>();

    DRY_COPY_BASE_ATTRIBUTES(Animatable);
    DRY_ACCESSOR_ATTRIBUTE("Color", GetColor, SetColor, Color, COLOR_MARKER_HIDDEN, AM_DEFAULT);
}

LevelMarker::LevelMarker(Context* context, unsigned id, const Vector2& position, const String& map): Animatable(context),
    id_{ id },
    position_{ position },
    map_{ map },
    color_{ COLOR_MARKER_HIDDEN },
    next_{ nullptr },
    previous_{ nullptr },
    route_{ nullptr },
    locked_{ true }
{}

float LevelMarker::GetViewYaw() const
{
    return position_.Angle(Vector2::DOWN) * Sign(position_.DotProduct(Vector2::RIGHT));
}

void LevelMarker::FadeToColor(const Color& color, float delay)
{
    const Color startColor{ GetColor() };
    ValueAnimation* colAnim{ new ValueAnimation{ context_ } };
    colAnim->SetKeyFrame(0.f, startColor);
    colAnim->SetKeyFrame(0.f + delay, startColor);
    colAnim->SetKeyFrame(.23f + delay, color);
    colAnim->SetInterpolationMethod(IM_LINEAR);

    SetAttributeAnimation("Color", colAnim, WM_CLAMP);
}

void LevelMarker::SetRoute(Route* route)
{
    route_ = route;

    if (route_)
    {
        if (next_)
            route_->SetDestination(next_->GetID());
        else
            route_->SetDestination(M_MAX_UNSIGNED);
    }
}

void LevelMarker::Unlock()
{
    if (!IsLocked())
        return;

    locked_ = false;

    if (route_)
        route_->Appear();
}

void LevelMarker::Reset()
{
    locked_ = true;
    RemoveAttributeAnimation("Color");
    SetColor(COLOR_MARKER_HIDDEN);

    if (route_)
        route_->Reset();
}

void LevelMarker::OnAttributeAnimationAdded()
{
    if (!scene_)
        return;

    if (attributeAnimationInfos_.Size() == 1)
        SubscribeToEvent(scene_, E_ATTRIBUTEANIMATIONUPDATE, DRY_HANDLER(LevelMarker, HandleAttributeAnimationUpdate));
}

void LevelMarker::OnAttributeAnimationRemoved()
{
    if (attributeAnimationInfos_.IsEmpty())
        UnsubscribeFromEvent(scene_, E_ATTRIBUTEANIMATIONUPDATE);
}

void LevelMarker::HandleAttributeAnimationUpdate(StringHash /*eventType*/, VariantMap& eventData)
{
    using namespace AttributeAnimationUpdate;

    UpdateAttributeAnimations(eventData[P_TIMESTEP].GetFloat());
}
