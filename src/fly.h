/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef FLY_H
#define FLY_H

#include "flyer.h"

class Rage;

class Fly: public Flyer
{
    DRY_OBJECT(Fly, Flyer);
public:
    static void RegisterObject(Context* context);

    Fly(Context* context);
    void Set(const Vector3& position, const Quaternion& rotation = Quaternion::IDENTITY) override;

    void Update(float timeStep) override;

protected:
    void OnNodeSet(Node* node) override;

private:
    void OnReleased(StringHash eventType, VariantMap& eventData);
    void OnCatched(StringHash eventType, VariantMap& eventData);

    Rage* rage_;
};

#endif // FLY_H
