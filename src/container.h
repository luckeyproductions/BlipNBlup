/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef CONTAINER_H
#define CONTAINER_H

#include "sceneobject.h"

class Catchable;

class Container: public SceneObject
{
    DRY_OBJECT(Container, SceneObject);

public:
    Container(Context* context);
    void Update(float timeStep) override;

    bool IsEmpty() const { return catchable_ == nullptr; }

    void Set(const Vector3& position, const Quaternion& rotation = Quaternion::IDENTITY) override;
    void Disable() override;
    Catchable* GetContents() const { return catchable_; }

protected:
    void OnNodeSet(Node* node) override;

    bool Contain(Catchable* catchable);
    virtual Catchable* Release(bool devoured);

    Node* graphicsNode_;
    StaticModel* model_;
    RigidBody* rigidBody_;
    CollisionShape* collider_;
    Catchable* catchable_;

private:
    bool escapable_;
    float containDuration_;
    float containTime_;
};

#endif // CONTAINER_H
