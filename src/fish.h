/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef PLAYER_H
#define PLAYER_H

#include "gamedefs.h"

#include "walker.h"

enum class FishState{ STAND, WALK, RUN, JUMP, FALL, OUT};

class Fish: public Walker
{
    DRY_OBJECT(Fish, Walker);

public:
    static void RegisterObject(Context* context);
    Fish(Context* context);

    void Set(const Vector3& position, const Quaternion& rotation) override;
    void BecomeBlip();
    void BecomeBlup();
    bool IsBlup() const { return isBlup_; };
    TeamPreference GetTeam() const { return (isBlup_ ? TP_BLUP : TP_BLIP); }
    void Faint();
    bool IsConscious() const { return !out_; }

    void Update(float timeStep) override;
    void FixedUpdate(float timeStep) override;

protected:
    void OnNodeSet(Node* node) override;
    void HandleAction(int actionId) override;

    void HandleNodeCollisionStart(StringHash eventType, VariantMap& eventData) override;

private:
    void Blink();
    void BlowBubble();
    void Kick();
    void PlayKickAnim(int side);
    RigidBody* DetermineKickHit(int& sideKick);
    Ray GetKickRay(float toRight = 0.f);
    void Revive();

    void HandlePostRenderUpdate(StringHash eventType, VariantMap& eventData);

    bool isBlup_;
    bool out_;
    float bubbleInterval_;
    float sinceBubble_;
    float kickInterval_;
    float sinceKick_;
    float blink_;

    String bubbleAnim_;
    Pair<String, String> kickAnim_;
};

DRY_EVENT(E_FISHFAINTED, FishFainted)
{
    DRY_PARAM(P_FISH, Fish); // ptr
}

#endif // PLAYER_H
