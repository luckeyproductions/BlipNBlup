/* Blip 'n Blup
// Copyright (C) 2018-2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "controllable.h"
#include "bnbcam.h"

#include "couch.h"

Player::Player(unsigned id): RefCounted(),
    id_{ id },
    joystickId_{ (id == 0u ? M_MAX_UNSIGNED : id - 1u) },
    preferredTeam_{ TP_RANDOM },
    controllable_{ nullptr },
    camera_{ nullptr }
{}

Player::~Player()
{
    if (controllable_)
        controllable_->ResetInput();
}

void Player::SetControl(Controllable* controllable, bool easeCamera)
{
    if (controllable_ == controllable)
    {
        if (controllable_ && !easeCamera)
            camera_->SetTarget(controllable_->GetNode(), !easeCamera);

        return;
    }

    if (controllable_)
        controllable_->ResetInput();

    controllable_ = controllable;

    if (!controllable_)
        return;

    Scene* controllableScene{ controllable_->GetScene() };
    if (!camera_)
    {
        Node* camNode{ controllableScene->CreateChild("Camera") };
        camera_ = camNode->CreateComponent<BnBCam>();
    }
    else
    {
        camera_->GetNode()->SetParent(controllableScene);
    }

    camera_->SetTarget(controllable_->GetNode(), !easeCamera);
}

Couch::Couch(Context* context): Object(context),
    players_{}
{
    for (unsigned p{ 0u }; p < Max(1u, INPUT->GetNumJoysticks()); ++p)
        AddPlayer();
}

Player* Couch::AddPlayer()
{
    unsigned id{ 1u };
    while (players_.Contains(id))
        ++id;

    players_[id] = MakeShared<Player>(id);

    return players_[id];
}

Player* Couch::GetPlayer(unsigned id) const
{
    if (!players_.Contains(id))
        return nullptr;
    else
        return *players_[id];
}

PODVector<Player*> Couch::GetPlayers() const
{
    PODVector<Player*> players{};

    for (Player* p: players_.Values())
        players.Push(p);

    return players;
}

PODVector<Player*> Couch::GetControllingPlayers() const
{
    PODVector<Player*> controllingPlayers{};
    for (Player* p: players_.Values())
    {
        if (p->GetControllable() != nullptr)
            controllingPlayers.Push(p);
    }

    return controllingPlayers;
}

unsigned Couch::GetNumControllingPlayers() const
{
    unsigned controlling{ 0u };
    for (Player* p: players_.Values())
    {
        if (p->GetControllable() != nullptr)
            ++controlling;
    }

    return controlling;
}

bool Couch::RemovePlayer(unsigned id)
{
    return players_.Erase(id);
}
