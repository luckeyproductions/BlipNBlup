/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "catchable.h"
#include "game.h"

#include "container.h"

Container::Container(Context* context): SceneObject(context),
    graphicsNode_{ nullptr },
    model_{ nullptr },
    rigidBody_{ nullptr },
    collider_{ nullptr },
    catchable_{ nullptr },
    escapable_{ true },
    containDuration_{ 1.f },
    containTime_{ containDuration_ }
{
    SetUpdateEventMask(USE_UPDATE);
}

void Container::OnNodeSet(Node *node)
{
    if (!node)
        return;

    graphicsNode_ = node_->CreateChild("Graphics");
    model_        = graphicsNode_->CreateComponent<StaticModel>();

    rigidBody_  = node_->CreateComponent<RigidBody>();
    collider_   = node_->CreateComponent<CollisionShape>();
}

void Container::Update(float timeStep)
{
    if (IsEmpty() || !escapable_)
        return;

    containTime_ -= timeStep;
    if (containTime_ < 0.f)
        Release(false);
}

void Container::Set(const Vector3& position, const Quaternion& rotation)
{
    SceneObject::Set(position, rotation);

    containTime_ = containDuration_;
    catchable_ = nullptr;
}

void Container::Disable()
{
    if (!IsEmpty())
        Release(false);

    SceneObject::Disable();
}

bool Container::Contain(Catchable* catchable)
{
    graphicsNode_->SetAttributeAnimation("Scale", nullptr);
    graphicsNode_->SetScale(1.f);

    if (catchable->CatchIn(this))
    {
        catchable_ = catchable;

        Difficulty difficulty{ GAME->GetDifficulty() };
        float multiplier;
        switch (difficulty)
        {
        case D_NORMAL: default: multiplier = 1.f; break;
        case D_EASY: multiplier = 1.5f; break;
        case D_HARD: multiplier =  .5f; break;
        }
        containTime_ = catchable_->GetReleaseTime() * multiplier;

        return true;
    }
    else
    {
        return false;
    }
}

Catchable* Container::Release(bool devoured)
{
    if (!IsEmpty())
    {
        Catchable* released{ catchable_ };
        catchable_->Release(devoured);
        catchable_ = nullptr;

        return released;
    }

    return nullptr;
}
