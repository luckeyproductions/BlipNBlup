/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "game.h"
#include "goal.h"
#include "spawnmaster.h"
#include "effectmaster.h"

#include "ball.h"

void Ball::RegisterObject(Context* context)
{
    context->RegisterFactory<Ball>();
}

Ball::Ball(Context* context): SceneObject(context),
    rigidBody_{ nullptr },
    model_{ nullptr },
    scored_{ false }
{
    SetUpdateEventMask(USE_UPDATE | USE_FIXEDUPDATE);
}

void Ball::OnNodeSet(Node* node)
{
    if (!node)
        return;

    SceneObject::OnNodeSet(node);

//    graphicsNode_->SetRotation({ Random(360.f), Random(360.f), Random(360.f) });
//    graphicsNode_->SetScale(.7f);
//    FX->ScaleTo(graphicsNode_, 1.f, .1f);

    model_ = node_->CreateComponent<StaticModel>();
    model_->SetModel(RES(Model, "Models/Ball.mdl"));
    model_->SetMaterial(RES(Material, "Materials/VColOutline.xml"));
    model_->SetCastShadows(true);

    rigidBody_ = node_->CreateComponent<RigidBody>();
    rigidBody_->SetCollisionLayer(L_BUBBLES);
    rigidBody_->SetCollisionMask(L_WORLD | L_CHARACTERS | L_BUBBLES);
    rigidBody_->SetFriction(.3f);
    rigidBody_->SetRollingFriction(.001f);
    rigidBody_->SetLinearRestThreshold(0.f);
    rigidBody_->SetMass(2/3.f);
    rigidBody_->SetLinearDamping(.23f);
    rigidBody_->SetAngularDamping(.42f);
    rigidBody_->SetRestitution(.8f);
    rigidBody_->SetCcdRadius(1/2.f);
    rigidBody_->SetCcdMotionThreshold(rigidBody_->GetCcdRadius());

    node_->CreateComponent<CollisionShape>()->SetSphere(1.f);

    SubscribeToEvent(node_, E_NODECOLLISIONSTART, DRY_HANDLER(Ball, HandleNodeCollision));
    SubscribeToEvent(node_, E_NODECOLLISION, DRY_HANDLER(Ball, HandleNodeCollision));
}

void Ball::Set(const Vector3& position, const Quaternion& rotation)
{
    SceneObject::Set(position, rotation);

    rigidBody_->ResetForces();
    rigidBody_->SetLinearVelocity(Vector3::ZERO);
    rigidBody_->SetAngularVelocity(Vector3::ZERO);
    rigidBody_->ReAddBodyToWorld();

    scored_ = false;
}

void Ball::Disable()
{
    SceneObject::Disable();
}

void Ball::Update(float timeStep)
{
    SceneObject::Update(timeStep);
}

void Ball::FixedUpdate(float timeStep)
{
    // Magnus effect
    const Vector3 v{ rigidBody_->GetLinearVelocity() };
    const Vector3 a{ rigidBody_->GetAngularVelocity() };
    const Vector3 spinCross{ a.CrossProduct(v) * 2.3f };
    rigidBody_->ApplyForce(spinCross * timeStep);
    rigidBody_->ApplyTorque(-a.Normalized() * spinCross.Length() * timeStep * rigidBody_->GetFriction());
}

void Ball::HandleNodeCollision(StringHash /*eventType*/, VariantMap& eventData)
{
    Node* otherNode{ static_cast<Node*>(eventData[NodeCollisionStart::P_OTHERNODE].GetPtr()) };
    if (!otherNode)
        return;

    btTransform ballTransform{};
    rigidBody_->getWorldTransform(ballTransform);
    const Vector3 ballPos{ ballTransform.getOrigin() };
    Goal* goal{ otherNode->GetComponent<Goal>() };
    if (!goal)
        goal = SPAWN->GetNearest<Goal>(ballPos, M_INFINITY);
    if (!goal || !goal->WithinBounds(ballPos))
        return;

    MemoryBuffer contacts{ eventData[NodeCollisionStart::P_CONTACTS].GetBuffer() };
    PODVector<Vector3> normals{};
    while (!contacts.IsEof())
    {
        /*Vector3 contactPosition{*/contacts.ReadVector3();//};
        normals.Push(contacts.ReadVector3());
        /*float contactDistance{*/contacts.ReadFloat();//};
        /*float contactImpulse{ */contacts.ReadFloat();//};
    }

    const Vector3 goalPos{ goal->GetNode()->GetWorldPosition() };
    const Vector3 toGoalCenter{ goalPos - ballPos };

    for (Vector3 n: normals)
    {
        if (toGoalCenter.DotProduct(n) > 0.f)
        {
            scored_ = true;
            Disable();

            TeamPreference side{ static_cast<TeamPreference>(goal->GetTeam() ^ 1) };
            VariantMap goalData{ { GoalScored::P_SIDE, side } };
            SendEvent(E_GOALSCORED, goalData);

            return;
        }
    }
}
