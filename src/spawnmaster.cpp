/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "game.h"
#include "container.h"
#include "flyer.h"
#include "walker.h"
#include "satong.h"
#include "ball.h"
#include "blockmap.h"

#include "fly.h"
#include "wasp.h"
#include "catchable.h"
#include "effectmaster.h"

#include "spawnmaster.h"

SpawnMaster::SpawnMaster(Context* context): Object(context),
    untilBugSpawn_{ 0.f }
{
}

void SpawnMaster::Activate()
{
    SubscribeToEvent(GAME->GetWorld().playScene_, E_SCENEUPDATE, DRY_HANDLER(SpawnMaster, HandleSceneUpdate));
}

void SpawnMaster::Deactivate()
{
    UnsubscribeFromAllEvents();
}

void SpawnMaster::Clear()
{
    const GameMode mode{ GAME->GetGameMode() };

    DisableAllDerived<Container>();
    DisableAll<Ball>();
    DisableAllDerived<Flyer>();

    if (mode == GM_SURVIVAL)
    {
        DisableAllDerived<Walker>("Fish");
    }
    else
    {
        DisableAllDerived<Walker>();
        DisableAll<Satong>();
    }
}

Scene* SpawnMaster::GetScene() const
{
    return GetSubsystem<Game>()->GetScene();
}

void SpawnMaster::Restart()
{
    Clear();
    Activate();
}

void SpawnMaster::HandleSceneUpdate(StringHash /*eventType*/, VariantMap& eventData)
{
    const float timeStep{ eventData[SceneUpdate::P_TIMESTEP].GetFloat() };

    const GameMode mode{ GAME->GetGameMode() };
    if (mode == GM_SURVIVAL)
    {
        const Vector3 mapExtents{ GAME->GetWorld().level_->GetExtents() };
        const float innerMapRadius{ .5f * Min(mapExtents.x_, mapExtents.z_) };
        int flyers{ CountActiveDerived<Flyer>() };

        untilBugSpawn_ -= timeStep;
        if ((untilBugSpawn_ <= 0.f && flyers < CeilToInt(Sqrt(2.f * innerMapRadius))) || flyers < 2)
        {
            const World& world{ GAME->GetWorld() };
            const Vector3 navPoint{ world.flyNav_->GetRandomPointInCircle(Vector3::ZERO, innerMapRadius * 2/3.f, mapExtents) };
            PhysicsWorld* physics{ world.playScene_->GetComponent<PhysicsWorld>() };
            PhysicsRaycastResult result{};
            const Ray groundRay{ navPoint.ProjectOntoPlane(Vector3::UP) + mapExtents.y_ * .125f * Vector3::UP, Vector3::DOWN };
            physics->SphereCast(result, groundRay, .5f, mapExtents.y_, L_WORLD);
            const Vector3 spawnPoint{ result.position_ + Vector3::UP * 2.f };
            const Quaternion rot{ Random(360.f), Vector3::UP };

            PODVector<RigidBody*> blockers{};
            if (spawnPoint.y_ < mapExtents.y_ && !physics->GetRigidBodies(blockers, { spawnPoint, 1.f }, L_WORLD | L_CHARACTERS))
            {
                Flyer* flyer{ nullptr };
                if (RandomBool())
                {
                    flyer = Create<Fly>(true, spawnPoint, rot);
                    flyer->GetComponent<Catchable>()->SetTeam(TP_RANDOM);
                }
                else
                    flyer = Create<Wasp>(true, spawnPoint, rot);

                if (flyer)
                {
                    Node* flyerNode{ flyer->GetNode() };
                    flyerNode->SetScale(0.f);
                    FX->ScaleTo(flyerNode, 1.f, .23f);
                }

                untilBugSpawn_ = .1f;//Random(1.f, 5.f);
            }
        }
    }
}
