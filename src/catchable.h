/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef CATCHABLE_H
#define CATCHABLE_H

#include "gamedefs.h"
#include "mastercontrol.h"

DRY_EVENT(E_CATCH, Catch){}
DRY_EVENT(E_BUGRELEASED, BugReleased)
{
    DRY_PARAM(P_CATCHABLE, Catchable);  // pointer
    DRY_PARAM(P_DEVOURER, Devourer);    // pointer
    DRY_PARAM(P_DEVOURED, Devoured);    // bool
}

class Container;

class Catchable: public LogicComponent
{
    DRY_OBJECT(Catchable, LogicComponent);
public:
    static void RegisterObject(Context* context);
    Catchable(Context* context);

    void SetReleaseTime(float time) { releaseTime_ = time; }
    float GetReleaseTime() { return releaseTime_; }
    bool IsCaught() const { return caught_; }
    bool CatchIn(Container* container);
    void Release(bool devoured);
    void SetNourishment(float nutrition) { nourishment_ = nutrition; }
    float GetNourishment() const { return nourishment_; }

    void SetTeam(TeamPreference team);
    TeamPreference GetTeam() const { return team_; }

protected:
    void OnNodeSet(Node* node) override;

private:
    float nourishment_;
    bool caught_;
    float releaseTime_;
    TeamPreference team_;
};

#endif // CATCHABLE_H
