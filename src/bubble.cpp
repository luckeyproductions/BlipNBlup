/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "game.h"
#include "wind.h"
#include "satong.h"
#include "fish.h"
#include "catchable.h"
#include "effectmaster.h"

#include "bubble.h"

void Bubble::RegisterObject(Context* context)
{
    context->RegisterFactory<Bubble>();
}

Bubble::Bubble(Context* context): Container(context),
    untilPop_{ 0.f },
    fromTop_{ false },
    fromBottom_{ false }
{
    SetUpdateEventMask(USE_UPDATE | USE_FIXEDPOSTUPDATE);
}

void Bubble::OnNodeSet(Node* node)
{
    if (!node)
        return;

    Container::OnNodeSet(node);

    node_->CreateComponent<Wind>();

    graphicsNode_->SetRotation({ Random(360.f), Random(360.f), Random(360.f) });
    graphicsNode_->SetScale(.7f);
    FX->ScaleTo(graphicsNode_, 1.f, .1f);

    model_->SetModel(RES(Model, "Models/Bubble.mdl"));
    model_->SetMaterial(RES(Material, "Materials/Bubble.xml"));
    model_->SetCastShadows(true);

    rigidBody_->SetCollisionLayer(L_BUBBLES);
    rigidBody_->SetCollisionMask(L_WORLD | L_CHARACTERS | L_BUBBLES | L_WATER);
    rigidBody_->SetFriction(.3f);
    rigidBody_->SetRollingFriction(.05f);
    rigidBody_->SetLinearRestThreshold(0.f);
    rigidBody_->SetCcdRadius(1/2.f);
    rigidBody_->SetCcdMotionThreshold(rigidBody_->GetCcdRadius());
    UpdateRigidBody();

    collider_->SetSphere(1.f);

    SubscribeToEvent(node_, E_NODECOLLISIONSTART, DRY_HANDLER(Bubble, HandleNodeCollisionStart));
    SubscribeToEvent(node_, E_NODECOLLISION, DRY_HANDLER(Bubble, HandleNodeCollision));
}

void Bubble::Set(const Vector3& position, const Quaternion& rotation)
{
    Container::Set(position, rotation);

    UpdateRigidBody(0.f);
    untilPop_ = Random(2.3f, 4.2f);
    fromTop_ = fromBottom_ = false;
}

void Bubble::Disable()
{
    Container::Disable();
}

void Bubble::Update(float timeStep)
{
    Container::Update(timeStep);

    if (IsEmpty())
        untilPop_ -= timeStep;
    if (untilPop_ <= 0.f)
        Disable();

    if (!IsEmpty())
        rigidBody_->ApplyForce((node_->GetChild(0u)->GetWorldPosition() - node_->GetPosition()) * timeStep * 420.f * rigidBody_->GetMass());
    else
        rigidBody_->ApplyTorque(timeStep * Vector3{ 13.f, 23.f, 34.f });
}

void Bubble::FixedPostUpdate(float /*timeStep*/)
{
    if (fromTop_ && fromBottom_)
    {
        Pop();
    }

    fromTop_ = fromBottom_ = false;
}

void Bubble::UpdateRigidBody(float otherMass)
{
    rigidBody_->DisableMassUpdate();
    if (otherMass == 0.f)
    {
        rigidBody_->SetMass(BUBBLE_WEIGHT);
        rigidBody_->SetLinearDamping(.9f);
        rigidBody_->SetAngularDamping(.1f);
        rigidBody_->SetGravityOverride(Vector3::UP * 9.f);
        rigidBody_->ResetForces();
        rigidBody_->SetAngularVelocity(Vector3::ZERO);
        rigidBody_->SetLinearVelocity(Vector3::ZERO);
        rigidBody_->SetRestitution(.125f);
        rigidBody_->ReAddBodyToWorld();
    }
    else
    {
        rigidBody_->SetMass(2/3.f * otherMass + BUBBLE_WEIGHT);
        rigidBody_->SetLinearDamping(.23f);
        rigidBody_->SetAngularDamping(.42f);
        rigidBody_->SetGravityOverride(Vector3::ZERO);
        rigidBody_->ResetForces();
        rigidBody_->SetAngularVelocity(Vector3::ZERO);
        rigidBody_->SetLinearVelocity(Vector3::ZERO);
        rigidBody_->SetRestitution(.8f);
        rigidBody_->ReAddBodyToWorld();
    }

    rigidBody_->EnableMassUpdate();
}

void Bubble::Devoured(Satong* satong)
{
    Release(satong);
}

void Bubble::HandleNodeCollisionStart(StringHash /*eventType*/, VariantMap& eventData)
{
    RigidBody* otherBody{ static_cast<RigidBody*>(eventData[NodeCollisionStart::P_OTHERBODY].GetPtr()) };
    Node* otherNode{ static_cast<Node*>(eventData[NodeCollisionStart::P_OTHERNODE].GetPtr()) };
    if (!otherNode || !otherBody)
        return;

    if (otherNode->HasTag("Pop"))
    {
        Pop();
        return;
    }

    MemoryBuffer contacts{ eventData[NodeCollisionStart::P_CONTACTS].GetBuffer() };
    /*Vector3 contactPosition{*/contacts.ReadVector3();//};
    /*Vector3 contactNormal{*/contacts.ReadVector3();//};
    /*float contactDistance{*/contacts.ReadFloat();//};
    float contactImpulse{ contacts.ReadFloat() };

    // See if devoured
    if (!IsEmpty())
    {
        Satong* satong{ otherNode->GetComponent<Satong>() };
        if (satong)
        {
            if (GAME->GetGameMode() != GM_CTF || catchable_->GetTeam() != satong->GetTeam())
            {
                satong->Devour(catchable_);
                Devoured(satong);
            }

            return;
        }
    }
    // Try to contain if catchable
    else
    {
        Catchable* catchable{ otherNode->GetComponent<Catchable>() };
        if (catchable && contactImpulse > .5f)
        {
            float otherMass{ otherBody->GetMass() };
            if (Contain(catchable))
            {
                UpdateRigidBody(otherMass);
                FX->TransformTo(node_,
                                .5f * (node_->GetPosition() + catchable->GetNode()->GetWorldPosition()),
                                node_->GetRotation(), .1f);
                return;
            }
        }
    }
}

void Bubble::HandleNodeCollision(StringHash /*eventType*/, VariantMap& eventData)
{
    Node* otherNode{ static_cast<Node*>(eventData[NodeCollisionStart::P_OTHERNODE].GetPtr()) };
    if (!otherNode)
        return;

    MemoryBuffer contacts{ eventData[NodeCollisionStart::P_CONTACTS].GetBuffer() };
    PODVector<Vector3> normals{};
    while (!contacts.IsEof())
    {
        /*Vector3 contactPosition{*/contacts.ReadVector3();//};
        normals.Push(contacts.ReadVector3());
        /*float contactDistance{*/contacts.ReadFloat();//};
        /*float contactImpulse{ */contacts.ReadFloat();//};
    }

    for (const Vector3& n: normals)
    {
        if (n.y_ > M_1_SQRT2)
            fromBottom_ = true;
        else if (n.y_ < -M_1_SQRT2 && otherNode->HasComponent<Fish>())
            fromTop_ = true;
    }
}

Catchable* Bubble::Release(bool devoured)
{
    Catchable* released{ Container::Release(devoured) };
    if (!devoured)
        PlaySample("EndBubble.wav");

    Disable();

    return released;
}

void Bubble::Pop()
{
    if (!IsEmpty())
        Release(false);
    else
        Disable();
}
