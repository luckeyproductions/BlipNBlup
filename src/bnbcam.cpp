/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "game.h"
#include "blockmap.h"
#include "fish.h"

#include "bnbcam.h"

void BnBCam::RegisterObject(Context* context)
{
    context->RegisterFactory<BnBCam>();
}

BnBCam::BnBCam(Context* context): LogicComponent(context),
    targetNode_{ nullptr },
    camera_{ nullptr },
    viewportIndex_{ 0u },
    lastTargetPos_{},
    sinceSwitch_{ 1.f },
    reflector_{}
{
}

void BnBCam::OnNodeSet(Node* node)
{
    if (!node)
        return;

    camera_ = node_->CreateComponent<Camera>();
    camera_->SetFarClip(1024.f);
    camera_->SetFov(90.f);
    node_->SetPosition({ 0.f, 7.0f, -10.0f });
    node_->SetRotation({ 23.f, 0.f, 0.f });

    node_->CreateComponent<SoundListener>();

    SubscribeToEvent(E_RESTART, DRY_HANDLER(BnBCam, HandleRestart));
}

void BnBCam::ClaimViewport(unsigned v)
{
    SharedPtr<Viewport> viewport{ RENDERER->GetViewport(v) };
    if (viewport)
    {
        viewport->SetScene(GetScene());
        viewport->SetCamera(camera_);
    }
    else
    {
        viewport = new Viewport{ context_, GetScene(), camera_ };
        RENDERER->SetViewport(v, viewport);
    }

    viewportIndex_ = v;
}

void BnBCam::SetReflectionsEnabled(bool enabled)
{
    if (!enabled)
    {
        if (reflector_.camera_)
            reflector_.SetUpdateMode(SURFACE_MANUALUPDATE);

        return;
    }

    Node* waterNode{ GAME->GetWorld().level_->GetWaterNode() };
    const Matrix3x4 waterTransform{ waterNode->GetWorldTransform() };
    const Plane waterPlane{ waterTransform.Rotation() * Vector3::UP, waterTransform.Translation() };
    const Plane waterClipPlane{ waterPlane.normal_, waterTransform.Translation() + .05f * Vector3::UP };

    unsigned camMask{ M_MAX_UNSIGNED };
    for (unsigned b{ 31u }; b >= 23u; --b)
        camMask ^= LAYER(b);

    reflector_.camera_ = node_->CreateComponent<Camera>();
    reflector_.camera_->SetFarClip(750.f);
    reflector_.camera_->SetViewMask(camMask); // Hide objects with only bit 31 in the viewmask (the water plane)
    reflector_.camera_->SetUseReflection(true);
    reflector_.camera_->SetReflectionPlane(waterPlane);
    reflector_.camera_->SetUseClipping(true);
    reflector_.camera_->SetClipPlane(waterClipPlane);
    reflector_.camera_->SetFov(camera_->GetFov());
    reflector_.camera_->SetAutoAspectRatio(false);

    const IntRect viewportRect{ RENDERER->GetViewport(viewportIndex_)->GetRect() };
    const float ratio{ 1.f * viewportRect.Width() / viewportRect.Height() };
    reflector_.camera_->SetAspectRatio(ratio);

    const unsigned textureSize{ Max(1u, NextPowerOfTwo(GRAPHICS->GetWidth()) / (CeilToInt(Sqrt(RENDERER->GetNumViewports())) + 1u)) };
    SharedPtr<Texture2D> renderTexture{ new Texture2D{ context_ } };
    renderTexture->SetSize(textureSize, textureSize, Graphics::GetRGBFormat(), TEXTURE_RENDERTARGET);
    renderTexture->SetFilterMode(FILTER_ANISOTROPIC);
    RenderSurface* surface{ renderTexture->GetRenderSurface() };
    SharedPtr<Viewport> rttViewport{ new Viewport{ context_, GetScene(), reflector_.camera_ } };
    surface->SetViewport(0, rttViewport);

    if (!reflector_.material_)
        reflector_.material_ = RES(Material, "Materials/Water.xml")->Clone("Reflection" + String{ viewportIndex_ });
    reflector_.material_->SetTexture(TU_DIFFUSE, renderTexture);

    unsigned waterMask{ LAYER(30u - viewportIndex_) };
    reflector_.water_ = waterNode->CreateComponent<StaticModel>();
    reflector_.water_->SetModel(RES(Model, "Models/Plane.mdl"));
    reflector_.water_->SetMaterial(reflector_.material_);
    reflector_.water_->SetViewMask(waterMask);

    camera_->SetViewMask(camMask | waterMask);
    reflector_.SetUpdateMode(SURFACE_UPDATEVISIBLE);

    SubscribeToEvent(E_BEGINRENDERING, DRY_HANDLER(BnBCam, HandleScreenMode));
    SubscribeToEvent(E_SCREENMODE, DRY_HANDLER(BnBCam, HandleScreenMode));
}

void BnBCam::Update(float timeStep)
{
    if (!targetNode_)
        return;

    float dt{ Clamp( 235.f * timeStep , 0.f, 1.f) };

    const Vector3 targetPos{ SinLerp(lastTargetPos_, targetNode_->GetPosition(), Min(1.f, sinceSwitch_)) };
    const float targetDistance{ node_->GetPosition().DistanceToPoint(targetPos) };
    const float minAltDelta{ 5.f };
    const float maxAltDelta{ 5.5f };
    float distance{ Lerp(targetDistance, 7.f, dt ) };

    const float altDelta{ node_->GetPosition().y_ - targetPos.y_ };
    if (altDelta < minAltDelta)
        node_->Translate(Vector3::UP * (minAltDelta - altDelta) * dt, TS_WORLD);
    else if (altDelta > maxAltDelta)
        node_->Translate(Vector3::UP * (maxAltDelta - altDelta) * dt, TS_WORLD);

    node_->LookAt(targetPos);

    const Vector3 targetDir{ targetNode_->GetWorldDirection() };
    float align{ node_->GetWorldDirection().CrossProduct(targetDir).y_ };
    node_->RotateAround(targetPos, { align * timeStep * 42.f, Vector3::UP}, TS_WORLD);

    PhysicsRaycastResult result{};
    Ray ray{ targetPos, (node_->GetPosition() - targetPos).Normalized() };
    const float radius{ .23f };
    float closest{ distance };
    PhysicsWorld* physics{ GetScene()->GetComponent<PhysicsWorld>() };
    if (physics->SphereCast(result, ray, radius, distance, L_WORLD))
    {
        float hitDistance{ result.position_.DistanceToPoint(node_->GetPosition()) };
        if (hitDistance < closest && hitDistance > .5f)
            distance = hitDistance;
    }

    if (targetDistance != distance)
        node_->Translate(Vector3::FORWARD * (targetDistance - distance) * dt  * .05f);

    lastTargetPos_ = targetPos;
    sinceSwitch_ += timeStep;
}

void BnBCam::JumpToNode(Node* node)
{
    node_->SetPosition(node->GetWorldPosition() + Quaternion{ node->GetWorldRotation().YawAngle(), Vector3::UP } * Vector3{ 0.f, 7.0f, -10.0f });
}

void BnBCam::SetTarget(Node* target, bool forceJump)
{
    GameMode mode{ GAME->GetGameMode() };

    if (!forceJump && mode == GM_STORY && targetNode_)
        sinceSwitch_ = 0.f;
    else if (target)
        JumpToNode(target);

    targetNode_ = target;
}

void BnBCam::JumpToTarget()
{
    if (targetNode_)
        JumpToNode(targetNode_);
}

void BnBCam::HandleRestart(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    JumpToTarget();
}

void BnBCam::HandleScreenMode(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    UpdateReflectionAspectRatio();

    if (HasSubscribedToEvent(E_BEGINRENDERING))
        UnsubscribeFromEvent(E_BEGINRENDERING);
}

void BnBCam::UpdateReflectionAspectRatio()
{
    if (reflector_.camera_)
        reflector_.camera_->SetAspectRatio(camera_->GetAspectRatio());
}
