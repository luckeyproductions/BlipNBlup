/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "wasp.h"
#include "catchable.h"
#include "rage.h"

Wasp::Wasp(Context* context): Flyer(context)
{
    maxFlySpeed_ = 13.f;
    flyThrust_ = 500.f;

    harmful_ = charger_ = true;
    chargeForce_ = 5000.f;
    chargeInterval_ = 5.f;

    SetUpdateEventMask(USE_UPDATE | USE_FIXEDUPDATE);
}

void Wasp::RegisterObject(Context* context)
{
    context->RegisterFactory<Wasp>();
}

void Wasp::OnNodeSet(Node *node)
{
    if (!node)
        return;

    Flyer::OnNodeSet(node);

    Catchable*  catchable{ node_->CreateComponent<Catchable>() };
    catchable->SetReleaseTime(17.f);
    SubscribeToEvent(node_, E_CATCH, DRY_HANDLER(Wasp, OnCatched));
    SubscribeToEvent(node_, E_BUGRELEASED, DRY_HANDLER(Wasp, OnReleased));

    rage_ = node_->CreateComponent<Rage>();
    rage_->SetCooldown(10.f);

    model_->SetModel(RES(Model, "Models/Wasp.mdl"));
    model_->SetMaterial(RES(Material, "Materials/VColOutline.xml")->Clone());

    rigidBody_->SetCollisionLayer(L_CHARACTERS);
    rigidBody_->SetCollisionMask(L_WORLD | L_CHARACTERS | L_BUBBLES);
    rigidBody_->SetMass(2.f);
    collider_->SetSphere(1.f);

    animCtrl_->PlayExclusive("Animations/Wasp_Fly.ani", 0, true, 0.f);
    animCtrl_->SetTime("Animations/Wasp_Fly.ani", randomizer_);
}

void Wasp::Set(const Vector3& position, const Quaternion& rotation)
{
    Flyer::Set(position, rotation);

    flyHeight_ = 2.5f;
    rage_->ResetAnger();
}

void Wasp::Update(float timeStep)
{
//    move_ = Quaternion{ 5.f - 2.f * rage_->GetAnger(), Vector3::UP } * node_->GetDirection(); ///Should be handled by AI component

    Flyer::Update(timeStep);

    animCtrl_->SetSpeed("Animations/Wasp_Fly.ani", 4.f + .2f * rigidBody_->GetLinearVelocity().Length());
    model_->GetMaterial()->SetShaderParameter("MatEmissiveColor", Color::RED * MC->Sine(2.3f, 0.f, .5f * rage_->GetAnger(), randomizer_));

    maxFlySpeed_ = 13.f + 7.f * rage_->GetAnger();
    flyThrust_ = 500.f + 200.f * rage_->GetAnger();
}

void Wasp::OnCatched(StringHash, VariantMap&)
{
    animCtrl_->PlayExclusive("Animations/Wasp_Caught.ani", 1u, true, .1f);
}

void Wasp::OnReleased(StringHash, VariantMap&)
{
    rage_->FillAnger();

    animCtrl_->StopLayer(1u);
    animCtrl_->PlayExclusive("Animations/Wasp_Fly.ani", 0, true, .1f);
    animCtrl_->SetSpeed("Animations/Wasp_Fly.ani", 5.f);
}
