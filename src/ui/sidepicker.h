/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef SIDEPICKER_H
#define SIDEPICKER_H

#include "gui.h"

class SidePicker: public UIElement
{
    DRY_OBJECT(SidePicker, UIElement);

public:
    static void RegisterObject(Context* context);
    SidePicker(Context* context);

    void CreateElements();
    void UpdateSizes();
    void SetOptions(StringVector options)
    {
        options_ = options;
        SetIndex(currentIndex_);
    }

    unsigned GetIndex() const { return currentIndex_; }
    PODVector<Button*> GetArrows() const { return arrows_; }

private:
    void SetIndex(unsigned index);
    void HandleArrowClicked(StringHash eventType, VariantMap& eventData);

    Text* optionText_;
    PODVector<Button*> arrows_;
    StringVector options_;
    unsigned currentIndex_;
};

#endif // SIDEPICKER_H
