/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "gamemenu.h"
#include "sidepicker.h"

SidePicker::SidePicker(Context* context): UIElement(context),
    optionText_{ nullptr },
    arrows_{},
    options_{},
    currentIndex_{ 0u }
{
    SetHorizontalAlignment(HA_CENTER);
    CreateElements();
}

void SidePicker::CreateElements()
{
    optionText_ = CreateChild<Text>();
    optionText_->SetName("OptionText");
    optionText_->SetText("Untitled");
    optionText_->SetAlignment(HA_CENTER, VA_CENTER);
    optionText_->SetFont(FONT_SUPER_BUBBLE);
    optionText_->SetColor(Color::AZURE.Lerp(Color::CYAN, .8f).Lerp(Color::WHITE, .2f));
    optionText_->SetTextEffect(TE_SHADOW);
    optionText_->SetEffectColor(Color::AZURE.Lerp(Color::BLUE, .25f).Transparent(.75f));
    optionText_->SetOpacity(.875f);

    for (bool left: { true, false })
    {
        Button* arrowButton{ CreateChild<Button>() };
        arrowButton->SetAlignment(HA_CENTER, VA_CENTER);
        arrowButton->SetTexture(RES(Texture2D, "UI/Arrow.png"));
        arrowButton->SetBlendMode(BLEND_ALPHA);
        arrowButton->SetImageRect({ 128 * !left, 0, 128 + 128 * !left, 256 });
        arrowButton->SetHoverOffset(0, 256);
        arrows_.Push(arrowButton);

        SubscribeToEvent(arrowButton, E_CLICKEND, DRY_HANDLER(SidePicker, HandleArrowClicked));

        if (!left)
        {
            Button* leftArrow{ arrows_.Front() };
            leftArrow->SetVar("Right", arrowButton);
            arrowButton->SetVar("Left", leftArrow);
        }
    }

    UpdateSizes();
}

void SidePicker::UpdateSizes()
{
    float fontSize{ Min(GUI::VW(3.4f), GUI::VH(5.5f)) };
    for (Button* arrowButton: arrows_)
    {
        arrowButton->SetSize(Min(1.5f * fontSize, 128), Min(3.f * fontSize, 256));
        arrowButton->SetPosition(9 * fontSize * (1 - 2 * (arrowButton == arrows_.Front())), -GUI::VH(1));
    }

    if (optionText_)
    {
        optionText_->SetFontSize(fontSize);
        optionText_->SetEffectShadowOffset({ 0, static_cast<int>(fontSize / 7) });
        optionText_->SetPosition(0, .23f * fontSize - GUI::VMin(2.3f));
    }
}

void SidePicker::SetIndex(unsigned index)
{
    if (options_.IsEmpty())
    {
        currentIndex_ = 0u;
        optionText_->SetText("");
        return;
    }

    const unsigned oldIndex{ currentIndex_ };
    currentIndex_ = Clamp(index, 0u, options_.Size() - 1u);
    optionText_->SetText(options_.At(currentIndex_));

    if (currentIndex_ != oldIndex)
    {
        VariantMap eventData{};
        eventData.Insert({ ItemSelected::P_ELEMENT, this });
        eventData.Insert({ ItemSelected::P_SELECTION, currentIndex_ });
        SendEvent(E_ITEMSELECTED, eventData);
    }
}

void SidePicker::HandleArrowClicked(StringHash /*eventType*/, VariantMap& eventData)
{
    UIElement* element{ static_cast<UIElement*>(eventData[Click::P_ELEMENT].GetPtr()) };
    if (GameMenu::Discard(eventData))
        return;

    unsigned index{ currentIndex_ };
    if (arrows_[0u] == element)
    {
        if (currentIndex_ > 0u)
            --index;
        else
            index = options_.Size() - 1u;
    }
    else
    {
        if (currentIndex_ < options_.Size() - 1u)
            ++index;
        else
            index = 0u;
    }

    if (currentIndex_ != index)
        SetIndex(index);
}
