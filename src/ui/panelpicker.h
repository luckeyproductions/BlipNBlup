/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef PANELPICKER_H
#define PANELPICKER_H

#include "gui.h"

class Panel: public Window
{
    DRY_OBJECT(Panel, UIElement);

public:
    Panel(Context* context);

    void SetTitle(const String& text);
    void SetPreview(Texture* preview);

private:
    void CreateTitle();
    void CreatePreview();

    Text* titleText_;
    BorderImage* previewImage_;
};

class PanelPicker: public UIElement
{
    DRY_OBJECT(PanelPicker, UIElement);

public:
    static void RegisterObject(Context* context);
    PanelPicker(Context* context);

    void CreateArrows();
    void UpdateSizes();

    void SetNumPanels(unsigned count);
    unsigned GetNumPanels() const { return panels_.Size(); }
    unsigned GetNumCells() const { return cells_; }

    unsigned GetPage() const { return page_; }
    void SetPage(unsigned page, bool setActive = true);

    PODVector<Panel*> GetPanels()   const { return panels_; }
    Panel* GetPanel(unsigned index) const { return panels_.At(index); }
    Panel* GetActivePanel()         const { return (GetNumPanels() == 0u ? nullptr : panels_.At(activePanel_)); }
    PODVector<Button*> GetArrows()  const { return arrows_; }

private:
    void SetActivePanel(unsigned index);

    void HandlePanelClicked(StringHash eventType, VariantMap& eventData);
    void HandleArrowClicked(StringHash eventType, VariantMap& eventData);

    PODVector<Panel*> panels_;
    PODVector<Button*> arrows_;
    unsigned cells_;
    unsigned page_;
    unsigned lastPage_;
    unsigned activePanel_;
};

DRY_EVENT(E_ACTIVEPANELCHANGED, ActivePanelChanged)
{
    DRY_PARAM(P_ELEMENT, Element);  // UIElement*
    DRY_PARAM(P_INDEX, Index);      // int
}

#endif // PANELPICKER_H
