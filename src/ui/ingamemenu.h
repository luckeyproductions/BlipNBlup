/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef INGAMEMENU_H
#define INGAMEMENU_H

#include "gamemenu.h"

enum InGameButton{ IGB_CONTINUE, IGB_SAVE, IGB_LOAD, IGB_RESTART, IGB_SETTINGS, IGB_MAIN, IGB_ALL };

class InGameMenu: public GameMenu
{
    const StringVector buttonsText_
    {
        { "Continue"    },
        { "Save"        },
        { "Load"        },
        { "Restart"     },
        { "Settings"    },
        { "Main Menu"   }
    };

    DRY_OBJECT(InGameMenu, GameMenu);

public:
    InGameMenu(Context* context);

    void CreateButtons() override;

protected:
    void UpdateSizes() override;

    void Back() override;
    void HandleVisibleChanged(StringHash eventType, VariantMap& eventData) override;

private:
    void UpdateButtonVisibility();

    void HandleContinueButtonClicked(StringHash, VariantMap& eventData);
    void HandleRestartButtonClicked(StringHash, VariantMap& eventData);
    void HandleSettingsButtonClicked(StringHash eventType, VariantMap& eventData);
    void HandleMainButtonClicked(StringHash, VariantMap& eventData);
};

#endif // INGAMEMENU_H
