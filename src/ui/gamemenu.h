/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef GAMEMENU_H
#define GAMEMENU_H

#include "gui.h"

class GameMenu: public UIElement
{
    DRY_OBJECT(GameMenu, UIElement);

public:
    static bool Discard(VariantMap& eventData);
    GameMenu(Context* context);

    virtual void CreateButtons() {}
    void HandleButtonClicked(StringHash eventType, VariantMap& eventData);

protected:
    Button* CreateTextButton(const String& text);
    void SetTextButtonEnabled(Button* button, bool enabled);

    virtual void UpdateSizes() {}
    virtual void Back() { activeElem_ = firstElem_; }
    UIElement* GetFirstElement() { return firstElem_; }

    virtual void HandleMenuInput(StringHash eventType, VariantMap& eventData);
    virtual void HandleVisibleChanged(StringHash eventType, VariantMap& eventData);
    void HandleBackButtonClicked(StringHash eventType, VariantMap& eventData);

    HashMap<int, Button*> buttons_;

    // Only used to determined focus element in absense of focus.
    UIElement* firstElem_;
    UIElement* lastElem_;
    UIElement* activeElem_;

private:
    void HandleSizesChanged(StringHash /*eventType*/, VariantMap& /*eventData*/)
    {
        UpdateSizes();
    }
};

#endif // GAMEMENU_H
