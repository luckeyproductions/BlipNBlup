/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../game.h"
#include "../jukebox.h"

#include "ingamemenu.h"

InGameMenu::InGameMenu(Context* context): GameMenu(context)
{

}

void InGameMenu::CreateButtons()
{
    for (int b{ 0 }; b < IGB_ALL; ++b)
    {
        String text{ buttonsText_.At(b) };
        Button* button{ CreateTextButton(text) };
        buttons_.Insert({ b, button });

        if (b == IGB_SETTINGS || b == IGB_SAVE || b == IGB_LOAD)
            SetTextButtonEnabled(button, false);

        if (b != 0)
        {
            buttons_[b - 1]->SetVar("Next", button);
            button->SetVar("Prev", buttons_[b - 1]);
        }

        if (b == IGB_ALL - 1)
        {
            button->SetVar("Next", buttons_[0]);
            buttons_[0]->SetVar("Prev", button);
        }
    }

    firstElem_ = buttons_[IGB_CONTINUE];
    lastElem_  = buttons_[IGB_MAIN];

    SubscribeToEvent(buttons_[IGB_CONTINUE], E_CLICKEND, DRY_HANDLER(InGameMenu, HandleContinueButtonClicked));
    SubscribeToEvent(buttons_[IGB_RESTART],  E_CLICKEND, DRY_HANDLER(InGameMenu, HandleRestartButtonClicked));
    SubscribeToEvent(buttons_[IGB_SETTINGS], E_CLICKEND, DRY_HANDLER(InGameMenu, HandleSettingsButtonClicked));
    SubscribeToEvent(buttons_[IGB_MAIN],     E_CLICKEND, DRY_HANDLER(InGameMenu, HandleMainButtonClicked));

    UpdateSizes();
}

void InGameMenu::UpdateSizes()
{
    SetSize(GUI::sizes_.screen_);

    int a{ 0 };
    for (int b{ 0 }; b < IGB_ALL; ++b)
    {
        Button* button{ buttons_[b] };
        if (!button || !button->IsVisible())
            continue;

        const float buttonHeight{ GUI::VMin(10) };
        button->SetSize(GUI::VMin(65), buttonHeight);
        button->SetPosition(0, Max(GUI::VH(17), GUI::VMin(23)) + (buttonHeight + GUI::VH(1)) * a);

        Text* buttonText{ button->GetChildStaticCast<Text>(0u) };
        float fontSize{ buttonHeight * .55f };
        buttonText->SetFontSize(fontSize);
        buttonText->SetPosition(0, -fontSize / 10);
//        buttonText->SetEffectStrokeThickness(1 + Ceil(fontSize / 23));
        buttonText->SetEffectShadowOffset({ 0, static_cast<int>(fontSize / 7) });

        ++a;
    }
}

void InGameMenu::Back()
{
    GetSubsystem<GUI>()->Show(nullptr);

    if (GAME->GetStatus() != GS_OVERWORLD)
        GAME->Unpause();
}

void InGameMenu::UpdateButtonVisibility()
{
    const GameMode mode{ GAME->GetGameMode() };
    const GameStatus status{ GAME->GetStatus() };

    for (Button* b: { buttons_[IGB_LOAD], buttons_[IGB_SAVE] })
        b->SetVisible(mode == GM_STORY);

    for (Button* b: { buttons_[IGB_CONTINUE], buttons_[IGB_SAVE] })
        SetTextButtonEnabled(b, status != GS_GAMEOVER);

    buttons_[IGB_RESTART]->SetVisible(status != GS_OVERWORLD);

    UpdateSizes();
}

void InGameMenu::HandleVisibleChanged(StringHash eventType, VariantMap& eventData)
{
    if (!IsVisible())
        return;

    UpdateButtonVisibility();

    activeElem_ = firstElem_;
    GameMenu::HandleVisibleChanged(eventType, eventData);
}

void InGameMenu::HandleContinueButtonClicked(StringHash /*eventType*/, VariantMap& eventData)
{
    if (Discard(eventData))
        return;

    Back();
}

void InGameMenu::HandleRestartButtonClicked(StringHash /*eventType*/, VariantMap& eventData)
{
    if (Discard(eventData))
        return;

    GetSubsystem<GUI>()->Show(nullptr);
    if (GAME->GetGameMode() > GM_STORY)
        GetSubsystem<Arcade>()->RestartMatch();
    else
        GetSubsystem<Game>()->RestartStoryLevel();

    GetSubsystem<JukeBox>()->RestartCurrent();
    GAME->Unpause();
}

void InGameMenu::HandleSettingsButtonClicked(StringHash /*eventType*/, VariantMap& eventData)
{
    if (Discard(eventData))
        return;

    SetVisible(false);
//    GetSubsystem<GUI>()->ShowSettings();
}

void InGameMenu::HandleMainButtonClicked(StringHash /*eventType*/, VariantMap& eventData)
{
    if (Discard(eventData))
        return;

    GetSubsystem<GUI>()->ShowMain();
}
