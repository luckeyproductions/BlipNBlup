/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef SPLASHSCREEN_H
#define SPLASHSCREEN_H

#include "gui.h"

#define ANIM_SPEED 2.f

class SplashScreen: public UIElement
{
    DRY_OBJECT(SplashScreen, UIElement);

public:
    SplashScreen(Context* context);

    void UpdateSizes();

    void Animate();
    void AnimateInitial(float time = 0.f);
    void AnimateSecondary(float time = 0.f);

private:
    float GetScale() const { return GUI::VMin(100.f) / 1080; }
    int FishFinalX(bool blip) const
    {
        const int halfWidth{ GUI::sizes_.screen_.x_ / 2 };
        const int finalX{ halfWidth + (blip ? -1 : 1) * Min(GUI::VWInt(30.f), GUI::VMinInt(42.f)) };

        return finalX;
    }

    void CreateElements();
    void SetPositionFrames(ObjectAnimation* objectAnim, const Vector<VAnimKeyFrame>& keyFrames, WrapMode wrapMode = WM_CLAMP) const;
    void HandlePressStartAppeared(StringHash eventType, VariantMap& eventData);
    void HandleKeyButtonDown(StringHash eventType, VariantMap& eventData);
    void HandleGameStatusChanged(StringHash eventType, VariantMap& eventData);

    BorderImage* blip_;
    BorderImage* blup_;
    BorderImage* blipText_;
    BorderImage* n_;
    BorderImage* blupText_;
    BorderImage* subtitle_;
    Sprite*      wasp_;
    Text*        pressStartText_;
};

DRY_EVENT(E_PRESSSTARTAPPEARED, PressStartAppeared) {}

#endif // SPLASHSCREEN_H
