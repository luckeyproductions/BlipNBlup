/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../game.h"

#include "storymenu.h"

StoryMenu::StoryMenu(Context* context): GameMenu(context),
    difficultyMenu_{ nullptr }
{
    context_->RegisterFactory<DifficultyMenu>();
}

void StoryMenu::CreateButtons()
{
    difficultyMenu_ = GetParent()->CreateChild<DifficultyMenu>();
    difficultyMenu_->CreateButtons();

    for (int b{ 0 }; b < SMB_ALL; ++b)
    {
        const String text{ buttonsText_.At(b) };
        Button* button{ CreateTextButton(text) };
        buttons_.Insert({ b, button });

        if (b == SMB_LOAD)
        {
            button->GetChildStaticCast<Text>(0u)->SetOpacity(.125f);
            button->SetEnabled(false);
        }

        if (b != 0)
        {
            buttons_[b - 1]->SetVar("Next", button);
            button->SetVar("Prev", buttons_[b - 1]);
        }

        if (b == SMB_ALL - 1)
        {
            button->SetVar("Next", buttons_[0]);
            buttons_[0]->SetVar("Prev", button);
        }
    }

    firstElem_ = buttons_[SMB_NEW];
    lastElem_  = buttons_[SMB_BACK];

    SubscribeToEvent(buttons_[SMB_NEW],  E_CLICKEND, DRY_HANDLER(StoryMenu, HandleNewButtonClicked));
    SubscribeToEvent(buttons_[SMB_LOAD], E_CLICKEND, DRY_HANDLER(StoryMenu, HandleLoadButtonClicked));
    SubscribeToEvent(buttons_[SMB_BACK], E_CLICKEND, DRY_HANDLER(StoryMenu, HandleBackButtonClicked));

    UpdateSizes();
}

void StoryMenu::UpdateSizes()
{
    SetSize(GUI::sizes_.screen_);

    for (int b{ 0 }; b < SMB_ALL; ++b)
    {
        Button* button{ buttons_[b] };

        if (!button)
            continue;

        const float buttonHeight{ GUI::VMin(10) };
        button->SetSize(GUI::VMin(65), buttonHeight);
        button->SetPosition(0, Max(GUI::VH(35), GUI::VMin(40)) + (buttonHeight + GUI::VH(1)) * b);

        Text* buttonText{ button->GetChildStaticCast<Text>(0u) };
        float fontSize{ buttonHeight * .55f };
        buttonText->SetFontSize(fontSize);
        buttonText->SetPosition(0, -fontSize / 10);
//        buttonText->SetEffectStrokeThickness(1 + Ceil(fontSize / 23));
        buttonText->SetEffectShadowOffset({ 0, static_cast<int>(fontSize / 7) });
    }
}

void StoryMenu::HandleNewButtonClicked(StringHash /*eventType*/, VariantMap& eventData)
{
    if (Discard(eventData))
        return;

    SetVisible(false);
    difficultyMenu_->SetVisible(true);
}

void StoryMenu::HandleLoadButtonClicked(StringHash /*eventType*/, VariantMap& eventData)
{
    if (Discard(eventData))
        return;

    SetVisible(false);
//    GetSubsystem<GUI>()->ShowSettings();
}

void StoryMenu::Back()
{
    GameMenu::Back();
    GetSubsystem<GUI>()->ShowMain();
}

DifficultyMenu::DifficultyMenu(Context* context): GameMenu(context)
{
}

void DifficultyMenu::CreateButtons()
{
    for (int b{ 0 }; b <= D_ALL; ++b)
    {
        const String text{ buttonsText_.At(b) };
        Button* button{ CreateTextButton(text) };
        buttons_.Insert({ b, button });

        if (b != 0)
        {
            buttons_[b - 1]->SetVar("Next", button);
            button->SetVar("Prev", buttons_[b - 1]);
        }

        if (b == D_ALL)
        {
            button->SetVar("Next", buttons_[0]);
            buttons_[0]->SetVar("Prev", button);
        }
        else
        {
            button->AddTag("Silent");
            button->SetVar("Difficulty", b);
        }

        SubscribeToEvent(button, E_CLICKEND, DRY_HANDLER(DifficultyMenu, HandleDifficultyButtonClicked));
    }

    firstElem_ = lastElem_ = buttons_[D_NORMAL];

    UpdateSizes();
}

void DifficultyMenu::UpdateSizes()
{
    SetSize(GUI::sizes_.screen_);

    for (int b{ 0 }; b <= D_ALL; ++b)
    {
        Button* button{ buttons_[b] };

        if (!button)
            continue;

        const float buttonHeight{ GUI::VMin(10) };
        button->SetSize(GUI::VMin(65), buttonHeight);
        button->SetPosition(0, Max(GUI::VH(35), GUI::VMin(40)) + (buttonHeight + GUI::VH(1)) * b);

        Text* buttonText{ button->GetChildStaticCast<Text>(0u) };
        float fontSize{ buttonHeight * .55f };
        buttonText->SetFontSize(fontSize);
        buttonText->SetPosition(0, -fontSize / 10);
//        buttonText->SetEffectStrokeThickness(1 + Ceil(fontSize / 23));
        buttonText->SetEffectShadowOffset({ 0, static_cast<int>(fontSize / 7) });
    }
}

void DifficultyMenu::Back()
{
    GameMenu::Back();

    SetVisible(false);
    GetSubsystem<GUI>()->ShowStory();
}

void DifficultyMenu::HandleDifficultyButtonClicked(StringHash /*eventType*/, VariantMap& eventData)
{
    if (Discard(eventData))
        return;

    Button* button{ static_cast<Button*>(eventData[ClickEnd::P_ELEMENT].GetPtr()) };
    if (!button)
        return;

    if (button == buttons_[D_ALL])
    {
        Back();
    }
    else
    {
        GetSubsystem<GUI>()->Show(nullptr);
        const Difficulty difficulty{ static_cast<Difficulty>(button->GetVar("Difficulty").GetInt()) };
        GetSubsystem<Game>()->StartNewStory(difficulty);
        SetVisible(false);
    }

}
