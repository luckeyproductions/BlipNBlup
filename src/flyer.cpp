/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "catchable.h"

#include "flyer.h"

Flyer::Flyer(Context* context): Controllable(context),
    flyHeight_{ 2.3f + randomizer_ },
    altitude_{ 2.f },
    charger_{ false },
    chargeForce_{},
    chargeInterval_{}
{
}

void Flyer::OnNodeSet(Node* node)
{
    if (!node)
        return;

    Controllable::OnNodeSet(node);

    rigidBody_->SetLinearRestThreshold(.01f);
    rigidBody_->SetLinearDamping(.25f);
    rigidBody_->SetRestitution(.23f);
    rigidBody_->SetAngularDamping(1.f);
    rigidBody_->SetRollingFriction(.5f);
    rigidBody_->SetAngularFactor(Vector3::UP);
}

void Flyer::Set(const Vector3& position, const Quaternion& rotation)
{
    Controllable::Set(position, rotation);

    CorrectAltitude(1.f);
}

void Flyer::Update(float timeStep)
{
    Controllable::Update(timeStep);

    if (node_->HasComponent<Catchable>()
     && node_->GetComponent<Catchable>()->IsCaught())
        return;

    CorrectAltitude(timeStep);
    AlignWithMovement(timeStep);
}

void Flyer::FixedUpdate(float timeStep)
{
    //Hover
    float forceY{ PowN(rigidBody_->GetMass(), 2) * ((altitude_ - node_->GetPosition().y_) * .25f - rigidBody_->GetLinearVelocity().y_ * .1f) };
    rigidBody_->ApplyForce(Vector3::UP * flyThrust_ * 17.f *
                           Sign(forceY) * Sqrt(Abs(forceY)) *
                           MC->Sine(1.f - .23f * randomizer_, 1.f, 2.f, randomizer_) * timeStep);
    //Fly
    const Vector3 flatVelocity{ rigidBody_->GetLinearVelocity().ProjectOntoPlane(Vector3::UP) };
    if (flatVelocity.Length() < maxFlySpeed_
        || (rigidBody_->GetLinearVelocity().Normalized() + move_.Normalized()).Length() < 1.f)
    {
        Vector3 force{ move_ * flyThrust_ };
        const float directionFactor{ (1.f - (flatVelocity.NormalizedOrDefault(node_->GetWorldDirection()).Angle(force.ProjectOntoPlane(Vector3::UP)) / 90.f)) };
        const float remainingFlySpeed{ maxFlySpeed_ - flatVelocity.Length() * directionFactor };
        const float maxThrust{ Max(flyThrust_ * remainingFlySpeed, 0.f) };

        if (force.Length() > maxThrust)
            force = force.Normalized() * maxThrust;

        rigidBody_->ApplyForce(force * timeStep);
    }
}

void Flyer::CorrectAltitude(float timeStep)
{
    PhysicsRaycastResult result{};
    Ray floorRay(node_->GetWorldPosition() + Vector3::UP * collider_->GetSize().y_ * .5f,
                 Vector3::DOWN);

    if (GetScene()->GetComponent<PhysicsWorld>()->SphereCast(result, floorRay, collider_->GetSize().x_ * .25f, 23.f, L_WORLD))
        altitude_ = Lerp(altitude_, result.position_.y_ + flyHeight_, timeStep * 17.f);
}
