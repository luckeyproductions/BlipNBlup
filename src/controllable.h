/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef CONTROLLABLE_H
#define CONTROLLABLE_H

#include "sceneobject.h"
#include <bitset>

#define NUM_CONTROLLABLE_ACTIONS 4

class Controllable: public SceneObject
{
    DRY_OBJECT(Controllable, SceneObject);

public:
    Controllable(Context* context);

    void Set(const Vector3& position, const Quaternion& rotation = Quaternion::IDENTITY) override;

    void Update(float timeStep) override;
    void Disable() override;

    AnimatedModel* GetModel() const { return model_; }
    void SetMove(const Vector3& move);
    Vector3 GetMove() const { return move_; }
    void SetActions(std::bitset<NUM_CONTROLLABLE_ACTIONS> actions);
    void ResetInput() { move_ = aim_ = Vector3::ZERO; actions_.reset(); }

    bool IsHarmful() const { return harmful_; }

protected:
    void OnNodeSet(Node* node) override;

    void ClampPitch(Quaternion& rot);
    void AlignWithVelocity(float timeStep);
    void AlignWithMovement(float timeStep);

    virtual void HandleAction(int /*actionId*/) {}

    Vector3 move_;
    Vector3 aim_;
    float maxPitch_;
    float minPitch_;

    std::bitset<NUM_CONTROLLABLE_ACTIONS> actions_;
    HashMap<int, float> actionSince_;

    Node* graphicsNode_;
    AnimatedModel* model_;
    RigidBody* rigidBody_;
    CollisionShape* collider_;
    AnimationController* animCtrl_;
    bool harmful_;
};

#endif // CONTROLLABLE_H
