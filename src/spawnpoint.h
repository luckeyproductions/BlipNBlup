/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef SPAWNPOINT_H
#define SPAWNPOINT_H

#include "gamedefs.h"

#include "sceneobject.h"

class SpawnPoint: public SceneObject
{
    DRY_OBJECT(SpawnPoint, SceneObject);

public:
    SpawnPoint(Context* context);

    TeamPreference GetTeam() const { return team_; }
    void SetTeam(TeamPreference team) { team_ = team; }

private:
    TeamPreference team_;
};

#endif // SPAWNPOINT_H
