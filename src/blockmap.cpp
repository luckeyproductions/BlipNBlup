/* Blip 'n Blup
// Copyright (C) 2024 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "game.h"
#include "container.h"
#include "flyer.h"
#include "fish.h"
#include "satong.h"
#include "base.h"
#include "goal.h"
#include "fly.h"
#include "wasp.h"
#include "spawnpoint.h"
#include "spawnmaster.h"

#include "blockmap.h"

void BlockMap::RegisterObject(Context* context)
{
    context->RegisterFactory<BlockMap>();
    context->RegisterFactory<Block>();
}

BlockMap::BlockMap(Context* context): Component(context),
    xmlFile_{ nullptr },
    mapSize_{},
    blockSize_{},
    musicTheme_{ JukeBox::MUSIC_NONE },
    blockGroups_{},
    waterNode_{ nullptr }
//    rigidBody_{ nullptr },
{
}

void BlockMap::OnNodeSet(Node* node)
{
    if (!node)
        return;

//    rigidBody_ = node_->CreateComponent<RigidBody>();
//    rigidBody_->SetFriction(.42f);
//    rigidBody_->SetRestitution(.1f);
//    rigidBody_->SetCollisionLayer(L_WORLD);
//    rigidBody_->SetCollisionMask(L_WORLD | L_CHARACTERS | L_BUBBLES);

    node_->CreateComponent<Navigable>();
}

void BlockMap::Clear()
{
    // Remove blocks
    node_->RemoveAllChildren();
    waterNode_ = nullptr;

    for (StaticModelGroup* blockGroup: blockGroups_.Values())
        blockGroup->Remove();
    blockGroups_.Clear();

    // Remove colliders
    PODVector<CollisionShape*> colliders{};
    GetComponents<CollisionShape>(colliders);
    for (CollisionShape* c: colliders)
        c->Remove();

    // Remove spawned objects not part of map node
    SpawnMaster* spawn{ GetSubsystem<SpawnMaster>() };
    if (spawn)
        spawn->Clear();
}

void BlockMap::LoadXMLFile(XMLFile* file)
{
    if (!file)
        return;

    const XMLElement mapElem{ file->GetRoot("blockmap") };
    if (mapElem.IsNull())
        return;

    /// OPTIMIZE: if (xmlFile_ == file) RELOAD (i.e. skip static objects);

    xmlFile_ = file;
    LoadXML(mapElem);
}

bool BlockMap::LoadXML(const XMLElement& mapElem)
{
    if (!Component::LoadXML(mapElem))
        return false;

    Clear();

    musicTheme_ = JukeBox::MUSIC_NONE;

    Game* game{ GetSubsystem<Game>() };
    const GameMode mode{ game->GetGameMode() };
    const bool story{ mode == GM_STORY };
    mapSize_   = mapElem.GetIntVector3("map_size");
    blockSize_ = mapElem.GetVector3("block_size");
    XMLElement blockSetRefElem{ mapElem.GetChild("blockset") };
    HashMap<unsigned, HashMap<unsigned, BlockInfo > > blockSets{};

    // Load blocksets
    while (blockSetRefElem)
    {
        const unsigned blockSetId{ blockSetRefElem.GetUInt("id") };
        const String blockSetName{ blockSetRefElem.GetAttribute("name") };

        DeriveMusicTheme(blockSetName);

        XMLFile* blockSetFile{ RES(XMLFile, blockSetName) };
        if (!blockSetFile)
            return false;

        XMLElement blockSetElem{ blockSetFile->GetRoot("blockset") };
        XMLElement blockElem{ blockSetElem.GetChild("block") };

        while (blockElem)
        {
            BlockInfo blockInfo{};

            const unsigned blockId{ blockElem.GetUInt("id") };
            blockInfo.id_ = blockId;
            blockInfo.name_ = blockElem.GetAttribute("name");
            blockInfo.model_ = blockElem.GetAttribute("model");

            if (blockElem.HasAttribute("material"))
            {
                blockInfo.materials_ = StringVector{ blockElem.GetAttribute("material") };
            }
            else
            {
                StringVector materials{};
                XMLElement matElem{ blockElem.GetChild("material") };
                while (!matElem.IsNull())
                {
                    materials.Push(matElem.GetAttribute("name"));
                    matElem = matElem.GetNext("material");
                }

                blockInfo.materials_ = materials;
            }

            blockSets[blockSetId][blockId] = blockInfo;
            blockElem = blockElem.GetNext("block");
        }

        blockSetRefElem = blockSetRefElem.GetNext("blockset");
    }

    XMLElement layerElem{ mapElem.GetChild("gridlayer") };

    while (layerElem)
    {
//        const int layerId{ layerElem.GetInt("id") };
        XMLElement blockElem{ layerElem.GetChild("gridblock") };

        while (blockElem)
        {
            const int blockSetId{ blockElem.GetInt("set") };
            const int blockId{ blockElem.GetInt("block") };
            const String blockName{ blockSets[blockSetId][blockId].name_ };
            const String modelName{ blockSets[blockSetId][blockId].model_ };
            const Vector3 blockPosition{ (blockElem.GetVector3("coords") - (mapSize_ - IntVector3::ONE) * .5f) * blockSize_ };
            const Quaternion blockRotation{ blockElem.GetQuaternion("rot") };
            PODVector<Material*> materials{};
            for (const String& materialName: blockSets[blockSetId][blockId].materials_)
                materials.Push(RES(Material, materialName));

            if (blockName.Contains("Coast") && !HasWater())
                AddWater();

            if (blockName == "Satong")
            {
                if (story || mode == GM_CTF || mode == GM_SURVIVAL)
                {
                    Satong* satong{ SPAWN->Create<Satong>() };
                    satong->Set(blockPosition + Vector3::DOWN * .5f * blockSize_.y_, blockRotation);
                    satong->GetNode()->SetParent(node_);
                }
            }
            else if (blockName == "Base")
            {
                if (mode == GM_CTF)
                {
                    Base* base{ SPAWN->Create<Base>() };
                    base->Set(blockPosition, blockRotation);
                    base->Initialize(RES(Model, modelName), materials);
                    base->GetNode()->SetParent(node_);
                }
            }
            else if (blockName == "Goal")
            {
                if (mode == GM_SOCCER)
                {
                    Goal* goal{ SPAWN->Create<Goal>() };
                    goal->Set(blockPosition, blockRotation);
                    goal->Initialize(RES(Model, modelName), materials);
                    goal->GetNode()->SetParent(node_);
                }
            }
            else if (modelName.Contains("Blip") || modelName.Contains("Blup"))
            {
                bool blip{ modelName.Contains("Blip") };
                const Vector3 spawnPos{ blockPosition + Vector3::UP * .5f };

                if (mode == GM_STORY)
                {
                    const World& world{ game->GetWorld() };

                    if (blip)
                        world.blip_->Set(spawnPos, blockRotation);
                    else
                        world.blup_->Set(spawnPos, blockRotation);
                }
                else
                {
                    SpawnPoint* spawnpoint{ SPAWN->Create<SpawnPoint>() };
                    spawnpoint->Set(spawnPos, blockRotation);
                    spawnpoint->GetNode()->SetParent(node_);

                    if (blip)
                        spawnpoint->SetTeam(TP_BLIP);
                    else
                        spawnpoint->SetTeam(TP_BLUP);
                }
            }
            else if (story && blockName == "Fly")
            {
                SPAWN->Create<Fly>(true, blockPosition, blockRotation);
            }
            else if (story && blockName == "Wasp")
            {
                SPAWN->Create<Wasp>(true, blockPosition, blockRotation);
            }
            else
            {
                Node* blockNode{ node_->CreateChild("Block") };
                blockNode->SetPosition(blockPosition);
                blockNode->SetRotation(blockRotation);
                Block* block{ blockNode->CreateComponent<Block>() };
                block->Initialize(RES(Model, modelName), materials);
            }

            blockElem = blockElem.GetNext("gridblock");
        }

        layerElem = layerElem.GetNext("gridlayer");
    }

    UpdateZoneParameters();

    return true;
}

StaticModelGroup* BlockMap::AddBlockGroup(unsigned blockHash, Model* blockModel, const PODVector<Material*>& materials)
{
    if (blockGroups_.Contains(blockHash))
    {
        return blockGroups_[blockHash];
    }
    else
    {
        StaticModelGroup* blockGroup = GetScene()->CreateComponent<StaticModelGroup>();
        blockGroup->SetModel(blockModel);
        blockGroup->SetCastShadows(true);
        if (materials.Size() == 1u)
            blockGroup->SetMaterial(materials.Front());
        else for (unsigned m{ 0u }; m < materials.Size(); ++m)
            blockGroup->SetMaterial(m, materials.At(m));

        blockGroups_[blockHash] = blockGroup;
        return blockGroup;
    }
}

void BlockMap::DeriveMusicTheme(const String& blockSetName)
{
    if (musicTheme_ != JukeBox::MUSIC_NONE)
        return;

    if (blockSetName.Contains("Beach"))
        musicTheme_ = JukeBox::MUSIC_BEACH;
    else if (blockSetName.Contains("Jungle"))
        musicTheme_ = JukeBox::MUSIC_JUNGLE;
    else if (blockSetName.Contains("Forest"))
        musicTheme_ = JukeBox::MUSIC_FOREST;
    else if (blockSetName.Contains("Snow"))
        musicTheme_ = JukeBox::MUSIC_SNOW;
    else if (blockSetName.Contains("Mountains"))
        musicTheme_ = JukeBox::MUSIC_MOUNTAINS;
    else if (blockSetName.Contains("Volcano"))
        musicTheme_ = JukeBox::MUSIC_VOLCANO;
    else if (blockSetName.Contains("Space"))
        musicTheme_ = JukeBox::MUSIC_SPACE;
    else if (blockSetName.Contains("Urban"))
        musicTheme_ = JukeBox::MUSIC_BONUS;
}

void BlockMap::UpdateZoneParameters()
{
    Zone* zone{ GetScene()->GetComponent<Zone>() };
    Skybox* sky{ GetScene()->GetComponent<Skybox>() };

    if (HasWater())
    {
        zone->SetFogStart(42.f);
        zone->SetFogEnd(123.f);
        zone->SetHeightFog(true);
        zone->SetFogHeight(waterNode_->GetWorldPosition().y_ - .8f);
        zone->SetFogHeightScale(3.4f);
    }
    else
    {
        zone->SetFogStart(23.f);
        zone->SetFogEnd(101.f);
        zone->SetHeightFog(false);
    }

    switch (musicTheme_)
    {
    default: zone->SetAmbientColor(Color::BLACK); break;
    case JukeBox::MUSIC_BEACH:
        zone->SetAmbientColor({ .61f, .62f, .63f });
        zone->SetFogColor(Color::WHITE.Lerp(Color::CYAN, .42f));
        sky->GetMaterial()->SetShaderParameter("MatDiffColor", Color::WHITE.Lerp(Color::CYAN, .34f));
    break;
    case JukeBox::MUSIC_FOREST:
        zone->SetAmbientColor({ .5f, .7f, .8f });
        zone->SetFogColor(Color::SPRINGGREEN.Lerp(Color::BLUE, .23f));
        sky->GetMaterial()->SetShaderParameter("MatDiffColor", zone->GetFogColor());
    break;
    case JukeBox::MUSIC_SPACE:
        zone->SetAmbientColor({ .7f, .7f, .7f });
        zone->SetFogColor(Color::BLUE.Lerp(Color::GRAY, .55f));
        sky->GetMaterial()->SetShaderParameter("MatDiffColor", Color::BLACK);
    break;
    }
}

void BlockMap::AddWater()
{
    if (waterNode_)
        return;

    waterNode_ = node_->CreateChild("Water");
    waterNode_->Translate(Vector3::DOWN * (mapSize_.y_ * blockSize_.y_ * .5f + .25f));
    waterNode_->SetScale({ 300.f, 1.f, 300.f });
    waterNode_->AddTag("Pop");
    waterNode_->CreateComponent<Obstacle>();
    RigidBody* waterBody{ waterNode_->CreateComponent<RigidBody>() };
    waterBody->SetCollisionLayerAndMask(L_WATER, L_BUBBLES);
    waterBody->SetTrigger(true);
    CollisionShape* waterCollider{ waterNode_->CreateComponent<CollisionShape>() };
    waterCollider->SetStaticPlane();

    Node* bottomNode{ waterNode_->CreateChild("Bottom") };
    bottomNode->Translate(Vector3::DOWN);
    StaticModel* bottomModel{ bottomNode->CreateComponent<StaticModel>() };
    bottomModel->SetShadowMask(0u);
    bottomModel->SetLightMask(0u);
    bottomModel->SetModel(RES(Model, "Models/Plane.mdl"));
}
