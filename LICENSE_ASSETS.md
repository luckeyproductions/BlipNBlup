# Blip 'n Blup asset licenses

## Used licenses
[Public domain](https://en.wikipedia.org/wiki/Public_domain), [CC0](https://creativecommons.org/publicdomain/zero/1.0/), [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/)/[4.0](https://creativecommons.org/licenses/by/4.0/), [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/), [OFL](https://openfontlicense.org/open-font-license-official-text/)

### Various
##### Author irrelevant
- CC0
    + Docs/*
    + Screenshots/*
    + Resources/Materials/*
    + Resources/Shaders/*
    + Resources/Techniques/*
    + Resources/RenderPaths/*
    + Resources/PostProcess/*
    + Resources/Textures/Ramp.*
    + Resources/Textures/Spot.*
    + Resources/Textures/*.xml
    + Resources/Maps/*.ebs
    + Resources/Textures/BrightDay1*
    + Resources/Textures/Skybox.xml
    
### Music
##### Kevin MacLeod
- CC-BY 3.0
    + Resources/Music/Kevin MacLeod - Beach Party.ogg [| Source |](https://freemusicarchive.org/music/Kevin_MacLeod/Global_Sampler/Beach_Party/)
    + Resources/Music/Kevin MacLeod - Faster Does It.* [| Source |](https://freemusicarchive.org/music/Kevin_MacLeod/Jazz_Sampler/Faster_Does_It/)
    + Resources/Music/Kevin MacLeod - W A Mozart Divertimento K131.* [| Source |](https://freemusicarchive.org/music/Kevin_MacLeod/Classical_Sampler/Divertimento_K131)

##### Spring Spring
- CC0
    + Resources/Music/Spring Spring - Forest (Jazz remix).ogg [| Source |](https://opengameart.org/content/forest-jazz-remix)

##### Philipp Weigl
- CC-BY 4.0
    + Resources/Music/Philipp Weigl - Western Shores.* [| Source |](http://freemusicarchive.org/music/Philipp_Weigl/Sound-trax/Philipp_Weigl_-_07_- _Western_Shores)

##### mirz
- CC-BY 4.0
    + Resources/Music/mirz - Space0.ogg
    
### Samples
##### Sam Greaves
- CC-BY 4.0
    + Resources/Samples/Ticking.wav [| Source |](https://freesound.org/people/GR3AVE5Y/sounds/169247/)

##### Modanung
- CC0
    + Resources/Samples/Bump.ogg
    + Resources/Samples/CatchFoe.wav
    + Resources/Samples/Drop.wav
    + Resources/Samples/EndBubble.wav
    + Resources/Samples/FreeFoe.wav
    + Resources/Samples/Hit.wav
    + Resources/Samples/Jump.wav
    + Resources/Samples/ShootBubble.wav
    
### 2D
#### Fonts
##### fsuarez913
- Public domain
    + Resources/Fonts/Super Bubble.ttf [| Source |](https://www.dafont.com/super-bubble.font)

##### Fredrick Brennan
- OFL
    + Resources/Fonts/Some Time Later.otf [| Source |](https://www.dafont.com/some-time-later.font)

##### Bluestype Studio
- Public domain
    + Resources/Fonts/Happy Memories.ttf [| Source |](https://www.dafont.com/happy-memories.font)

##### Typodermic Fonts
- CC0
    + Resources/Fonts/Dream Orphans.otf [| Source |](https://www.dafont.com/dream-orphans.font)
    
#### Textures
##### Modanung
- CC-BY-SA 4.0
    + Resources/icon.png
    + Resources/Textures/Dash.png
    + Resources/Textures/Marker.png
    + Resources/Textures/Island.png
    + Resources/Textures/WaterNoise.dds
    + raw/*
    
### 3D
##### Modanung
- CC-BY-SA 4.0
    + Resources/Animations/*
    + Resources/Blocks/*
    + Resources/Models/*
    + Blends/*
    
## Maps
##### Modanung
- CC-BY 4.0
    + Maps/*
